package dsmr

import (
	"fmt"
	"testing"
	"time"
)

type testCase struct {
	Telegram                    string
	Prefix, Identification, CRC string
	Data                        map[string][]string
}

var tests = map[string]testCase{
	"DSMR 2.2": {
		Telegram:       "/KMP5 ZABF001587315111\r\n\r\n0-0:96.1.1(205C4D246333034353537383234323121)\r\n1-0:1.8.1(00185.000*kWh)\r\n1-0:1.8.2(00084.000*kWh)\r\n1-0:2.8.1(00013.000*kWh)\r\n1-0:2.8.2(00019.000*kWh)\r\n0-0:96.14.0(0001)\r\n1-0:1.7.0(0000.98*kW)\r\n1-0:2.7.0(0000.00*kW)\r\n0-0:17.0.0(999*A)\r\n0-0:96.3.10(1)\r\n0-0:96.13.1()\r\n0-0:96.13.0()\r\n0-1:24.1.0(3)\r\n0-1:96.1.0(3238313031453631373038389930337131)\r\n0-1:24.3.0(120517020000)(08)(60)(1)(0-1:24.2.1)(m3)\r\n(00124.477)\r\n0-1:24.4.0(1)\r\n!\r\n",
		Prefix:         "KMP5",
		Identification: "ZABF001587315111",
		Data: map[string][]string{
			fmt.Sprintf(EquipmentIdentifier, 0):                             {"205C4D246333034353537383234323121"},
			fmt.Sprintf(MeterReadingElectricityDeliveredToClientTariff1, 0): {"00185.000*kWh"},
			fmt.Sprintf(MeterReadingElectricityDeliveredToClientTariff2, 0): {"00084.000*kWh"},
			fmt.Sprintf(MeterReadingElectricityDeliveredByClientTariff1, 0): {"00013.000*kWh"},
			fmt.Sprintf(MeterReadingElectricityDeliveredByClientTariff2, 0): {"00019.000*kWh"},
			fmt.Sprintf(TariffIndicatorElectricity, 0):                      {"0001"},
			fmt.Sprintf(ActualElectricityPowerDelivered, 0):                 {"0000.98*kW"},
			fmt.Sprintf(ActualElectricityPowerReceived, 0):                  {"0000.00*kW"},
			fmt.Sprintf(MaxCurrentPerPhase, 0):                              {"999*A"},
			fmt.Sprintf(SwitchPosition, 0):                                  {"1"},
			fmt.Sprintf(TextMessage1, 0):                                    {""},
			fmt.Sprintf(TextMessage0, 0):                                    {""},
			fmt.Sprintf(OtherDeviceType, 1):                                 {"3"},
			fmt.Sprintf(OtherEquipmentIdentifier, 1):                        {"3238313031453631373038389930337131"},
			fmt.Sprintf(GasCombinedMeterValue, 1):                           {"120517020000", "08", "60", "1", "0-1:24.2.1", "m3", "00124.477"},
			fmt.Sprintf(GasValvePosition, 1):                                {"1"},
			// Postprocessing entries
			fmt.Sprintf(DateTimeStamp, 0):   {time.Now().Format(DateFormat)},
			fmt.Sprintf(OtherMeterValue, 1): {"120517020000", "00124.477*m3"},
		},
	},
	"DSMR 4.0": {
		Telegram:       "/ISk5\\2MT382-1 000\r\n\r\n1-3:0.2.8(40)\r\n0-0:1.0.0(101209113020W)\r\n0-0:96.1.1(4B384547303034303436333935353037)\r\n1-0:1.8.1(123456.789*kWh)\r\n1-0:1.8.2(123456.789*kWh)\r\n1-0:2.8.1(123456.789*kWh)\r\n1-0:2.8.2(123456.789*kWh)\r\n0-0:96.14.0(0002)\r\n1-0:1.7.0(01.1 93*kW)\r\n1-0:2.7.0(00.000*kW)\r\n0-0:17.0.0(016.1 *kW)\r\n0-0:96.3.10(1)\r\n0-0:96.7.21(00004)\r\n0-0:96.7.9(00002)\r\n1-0:99:97.0(2)(0:96.7.1 9)(101208152415W)(0000000240*s)(101208151004W)(00000000301*s)\r\n1-0:32.32.0(00002)\r\n1-0:52.32.0(00001)\r\n1-0:72.32.0(00000)\r\n1-0:32.36.0(00000)\r\n1-0:52.36.0(00003)\r\n1-0:72.36.0(00000)\r\n0-0:96.13.1(3031203631203831)\r\n0-0:96.13.0(303132333435363738393A3B3C3D3E3F303132333435363738393A3B3C3D3E3F303132333435363738393A3B\r\n3C3D3E3F303132333435363738393A3B3C3D3E3F303132333435363738393A3B3C3D3E3F)\r\n0-1:24.1.0(03)\r\n0-1:96.1.0(3232323241424344313233343536373839)\r\n0-1:24.2.1(101209110000W)(12785.123*m3)\r\n0-1:24.4.0(1)\r\n!522B",
		Prefix:         "ISk5",
		Identification: "\\2MT382-1 000",
		CRC:            "522B",
		Data: map[string][]string{
			fmt.Sprintf(VersionInformation, 3):                              {"40"},
			fmt.Sprintf(DateTimeStamp, 0):                                   {"101209113020W"},
			fmt.Sprintf(EquipmentIdentifier, 0):                             {"4B384547303034303436333935353037"},
			fmt.Sprintf(MeterReadingElectricityDeliveredToClientTariff1, 0): {"123456.789*kWh"},
			fmt.Sprintf(MeterReadingElectricityDeliveredToClientTariff2, 0): {"123456.789*kWh"},
			fmt.Sprintf(MeterReadingElectricityDeliveredByClientTariff1, 0): {"123456.789*kWh"},
			fmt.Sprintf(MeterReadingElectricityDeliveredByClientTariff2, 0): {"123456.789*kWh"},
			fmt.Sprintf(TariffIndicatorElectricity, 0):                      {"0002"},
			fmt.Sprintf(ActualElectricityPowerDelivered, 0):                 {"01.1 93*kW"},
			fmt.Sprintf(ActualElectricityPowerReceived, 0):                  {"00.000*kW"},
			fmt.Sprintf(MaxCurrentPerPhase, 0):                              {"016.1 *kW"},
			fmt.Sprintf(SwitchPosition, 0):                                  {"1"},
			fmt.Sprintf(NumberOfPowerFailuresInAnyPhases, 0):                {"00004"},
			fmt.Sprintf(NumberOfLongPowerFailuresInAnyPhases, 0):            {"00002"},
			fmt.Sprintf(PowerFailureEventLog, 0):                            {"2", "0:96.7.1 9", "101208152415W", "0000000240*s", "101208151004W", "00000000301*s"},
			fmt.Sprintf(NumberOfVoltageSagsInPhaseL1, 0):                    {"00002"},
			fmt.Sprintf(NumberOfVoltageSagsInPhaseL2, 0):                    {"00001"},
			fmt.Sprintf(NumberOfVoltageSagsInPhaseL3, 0):                    {"00000"},
			fmt.Sprintf(NumberOfVoltageSwellsInPhaseL1, 0):                  {"00000"},
			fmt.Sprintf(NumberOfVoltageSwellsInPhaseL2, 0):                  {"00003"},
			fmt.Sprintf(NumberOfVoltageSwellsInPhaseL3, 0):                  {"00000"},
			fmt.Sprintf(TextMessage1, 0):                                    {"3031203631203831"},
			fmt.Sprintf(TextMessage0, 0):                                    {"303132333435363738393A3B3C3D3E3F303132333435363738393A3B3C3D3E3F303132333435363738393A3B\r\n3C3D3E3F303132333435363738393A3B3C3D3E3F303132333435363738393A3B3C3D3E3F"},
			fmt.Sprintf(OtherDeviceType, 1):                                 {"03"},
			fmt.Sprintf(OtherEquipmentIdentifier, 1):                        {"3232323241424344313233343536373839"},
			fmt.Sprintf(OtherMeterValue, 1):                                 {"101209110000W", "12785.123*m3"},
			fmt.Sprintf(GasValvePosition, 1):                                {"1"},
		},
	},
}

func compareData(expect, actual map[string][]string) error {
	for k, exp := range expect {
		act, ok := actual[k]
		if !ok {
			return fmt.Errorf("expected key %q is not present", k)
		}
		if len(exp) != len(act) {
			return fmt.Errorf("different values for %q: expected %v, got %v", k, exp, act)
		}
		for i := range exp {
			if exp[i] != act[i] {
				return fmt.Errorf("different values for %q: expected %v, got %v", k, exp, act)
			}
		}
		delete(actual, k)
	}

	for k, exp := range actual {
		return fmt.Errorf("unexpected key %q with values %v", k, exp)
	}

	return nil
}

func TestParse(t *testing.T) {
	for name, tc := range tests {
		telegram, err := Parse(tc.Telegram)
		if err != nil {
			t.Errorf("[%s] failed: %s", name, err)
			continue
		}

		if tc.Prefix != telegram.Prefix {
			t.Errorf("[%s] prefix mismatch: expected %q, got %q", name, tc.Prefix, telegram.Prefix)
		}

		if tc.Identification != telegram.Identification {
			t.Errorf("[%s] identification mismatch: expected %q, got %q", name, tc.Identification, telegram.Identification)
		}

		if tc.CRC != telegram.CRC {
			t.Errorf("[%s] CRC mismatch: expected %q, got %q", name, tc.CRC, telegram.CRC)
		}

		err = compareData(tc.Data, telegram.Data)
		if err != nil {
			t.Errorf("[%s] failed: %s", name, err)
			continue
		}
	}
}

var testInts = []struct {
	Input string
	Value int
	Fail  bool
}{
	{"1", 1, false},
	{"fail", 0, true},
}

func TestTelegram_GetInt(t *testing.T) {
	for _, test := range testInts {
		telegram := &Telegram{Data: map[string][]string{"0": {test.Input}}}
		value, err := telegram.GetInt(0, "%d")
		if err != nil {
			if !test.Fail {
				t.Errorf("Error getting int: %s", err)
			}
			continue
		}
		if test.Value != value {
			t.Errorf("Incorrect int: expected %v, got %v", test.Value, value)
		}
	}
}

var testFloats = []struct {
	Input string
	Value float64
	Unit  string
	Fail  bool
}{
	{"00185.000*kWh", 00185.000, "kWh", false},
	{"fail", 0, "", true},
}

func TestTelegram_GetFloat(t *testing.T) {
	for _, test := range testFloats {
		telegram := &Telegram{Data: map[string][]string{"0": {test.Input}}}
		value, unit, err := telegram.GetFloat(0, "%d")
		if err != nil {
			if !test.Fail {
				t.Errorf("Error getting float: %s", err)
			}
			continue
		}
		if test.Value != value || test.Unit != unit {
			t.Errorf("Incorrect float: expected %v %q, got %v %q",
				test.Value, test.Unit, value, unit)
		}
	}
}

var testTimes = []struct {
	Input string
	Value time.Time
	Fail  bool
}{
	{"120517020000", time.Date(2012, 05, 17, 02, 00, 00, 00, time.Local), false},
	{"101209110000W", time.Date(2010, 12, 9, 11, 00, 00, 00, time.Local), false},
	{"fail", time.Time{}, true},
	{"longerfailstring", time.Time{}, true},
}

func TestTelegram_GetTime(t *testing.T) {
	for _, test := range testTimes {
		telegram := &Telegram{Data: map[string][]string{"0": {test.Input}}}
		value, err := telegram.GetTime(0, "%d")
		if err != nil {
			if !test.Fail {
				t.Errorf("Error getting time: %s", err)
			}
			continue
		}
		if test.Value != value {
			t.Errorf("Incorrect time: expected %v, got %v", test.Value, value)
		}
	}
}

var testMeterValues = map[string]map[int]struct {
	Value float64
	Unit  string
	Time  time.Time
	Fail  bool
}{
	"DSMR 2.2": {
		1: {00124.477, "m3", time.Date(2012, 05, 17, 02, 00, 00, 00, time.Local), false},
		2: {0, "", time.Time{}, true},
	},
	"DSMR 4.0": {
		1: {12785.123, "m3", time.Date(2010, 12, 9, 11, 00, 00, 00, time.Local), false},
		2: {0, "", time.Time{}, true},
	},
}

func TestTelegram_GetMeterValue(t *testing.T) {
	for testCase, devices := range testMeterValues {
		telegram, _ := Parse(tests[testCase].Telegram)
		for dev, test := range devices {
			ts, v, u, err := telegram.GetMeterValue(dev)
			if err != nil {
				if !test.Fail {
					t.Errorf("%s device %v error: %s", testCase, dev, err)
				}
				continue
			}
			if test.Time != ts {
				t.Errorf("%s device %v incorrect timestamp: expected %s, got %s",
					testCase, dev, test.Time, ts)
			}
			if test.Value != v {
				t.Errorf("%s device %v incorrect value: expected %v, got %v",
					testCase, dev, test.Value, v)
			}
			if test.Unit != u {
				t.Errorf("%s device %v incorrect unit: expected %s, got %s",
					testCase, dev, test.Unit, u)
			}
		}
	}
}
