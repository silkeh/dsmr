module gitlab.com/silkeh/dsmr

go 1.14

require (
	github.com/influxdata/influxdb1-client v0.0.0-20200515024757-02f0bf5dbca3
	github.com/tarm/serial v0.0.0-20180830185346-98f6abe2eb07
	golang.org/x/sys v0.0.0-20200802091954-4b90ce9b60b3 // indirect
)
