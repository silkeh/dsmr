// Package dsmr implements somewhat compatible DSMR parsing.
// For details on DSMR, see: https://www.netbeheernederland.nl/_upload/Files/Slimme_meter_15_a727fce1f1.pdf
package dsmr

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"
	"time"
)

const (
	// DateFormat represents the time format used in DSMR telegrams
	DateFormat = "060102150405"
)

// OBIS references for electricity data.
const (
	VersionInformation                              = "1-%d:0.2.8"
	DateTimeStamp                                   = "0-%d:1.0.0"
	EquipmentIdentifier                             = "0-%d:96.1.1"
	TariffIndicatorElectricity                      = "0-%d:96.14.0"
	SwitchPosition                                  = "0-%d:96.3.10" // non-standard
	MaxCurrentPerPhase                              = "0-%d:17.0.0"  // non-standard
	MeterReadingElectricityDeliveredToClientTariff1 = "1-%d:1.8.1"   // low tariff
	MeterReadingElectricityDeliveredToClientTariff2 = "1-%d:1.8.2"   // normal tariff
	MeterReadingElectricityDeliveredByClientTariff1 = "1-%d:2.8.1"   // low tariff
	MeterReadingElectricityDeliveredByClientTariff2 = "1-%d:2.8.2"   // normal tariff
	ActualElectricityPowerDelivered                 = "1-%d:1.7.0"
	ActualElectricityPowerReceived                  = "1-%d:2.7.0"
	NumberOfPowerFailuresInAnyPhases                = "0-%d:96.7.21"
	NumberOfLongPowerFailuresInAnyPhases            = "0-%d:96.7.9"
	PowerFailureEventLog                            = "1-%d:99:97.0"
	NumberOfVoltageSagsInPhaseL1                    = "1-%d:32.32.0"
	NumberOfVoltageSagsInPhaseL2                    = "1-%d:52.32.0"
	NumberOfVoltageSagsInPhaseL3                    = "1-%d:72.32.0"
	NumberOfVoltageSwellsInPhaseL1                  = "1-%d:32.36.0"
	NumberOfVoltageSwellsInPhaseL2                  = "1-%d:52.36.0"
	NumberOfVoltageSwellsInPhaseL3                  = "1-%d:72.36.0"
	InstantaneousVoltageL1                          = "1-%d:32.7.0"
	InstantaneousVoltageL2                          = "1-%d:52.7.0"
	InstantaneousVoltageL3                          = "1-%d:72.7.0"
	InstantaneousCurrentL1                          = "1-%d:31.7.0"
	InstantaneousCurrentL2                          = "1-%d:51.7.0"
	InstantaneousCurrentL3                          = "1-%d:71.7.0"
	InstantaneousActivePowerL1Pos                   = "1-%d:21.7.0"
	InstantaneousActivePowerL2Pos                   = "1-%d:41.7.0"
	InstantaneousActivePowerL3Pos                   = "1-%d:61.7.0"
	InstantaneousActivePowerL1Neg                   = "1-%d:22.7.0"
	InstantaneousActivePowerL2Neg                   = "1-%d:42.7.0"
	InstantaneousActivePowerL3Neg                   = "1-%d:62.7.0"
)

// OBIS references for messages
const (
	TextMessage0 = "0-%d:96.13.0"
	TextMessage1 = "0-%d:96.13.1"
)

// OBIS references for other meters (gas, thermal, water)
const (
	OtherDeviceType          = "0-%d:24.1.0"
	OtherEquipmentIdentifier = "0-%d:96.1.0"
	OtherMeterValue          = "0-%d:24.2.1"
)

// Other OBIS references
const (
	GasCombinedMeterValue = "0-%d:24.3.0" // non-standard, contains measurement date as first value.
	GasValvePosition      = "0-%d:24.4.0" // non-standard
)

var (
	// TelegramRegexp matches a telegram as per the DSMR protocol:
	//     /XXX5 Ident CR LF CR LF Data ! CRC CR LF
	// It is allowed to leave out any CR characters, the CRC and the final CRLF.
	TelegramRegexp = regexp.MustCompile(`\/\s*(\w{3}\d) *(.*?)\r?\n\r?\n((?s).*)\r?\n!(\w*)\r?\n?`)
	//TelegramRegexp = regexp.MustCompile(`\/\s*(\w{3}\d) *(\w)+\r?\n\r?\n`)

	// ObjectRegexp matches an object as per the DSMR protocol. This may look as follows:
	//     0-1:24.3.0(200801230000)(08)(60)(1)(0-1:24.2.1)(m3)\n(06085.734)
	//     1-0:99:97.0(2)(0:96.7.1 9)(101208152415W)(0000000240*s)(101208151004W)(00000000301*s)
	ObjectRegexp = regexp.MustCompile(`(\d+-\d[.:]\d+[.:]\d+[.:]\d+)((\((?s).*?\)\s*)+)`)

	// valueRegexp a single value from the value part of an object (enclosed in braces).
	valueRegexp = regexp.MustCompile(`\(((?s).*?)\)`)
)

// Telegram represents a DSMR telegram.
type Telegram struct {
	// Prefix contains the telegram starting prefix, eg: KMP5.
	Prefix string

	// Identification contains the message identification.
	Identification string

	// Data contains the message data objects.
	Data map[string][]string

	// CRC contains the telegram checksum. May be blank.
	CRC string
}

// Parse returns a new telegram from a telegram string.
// The given string has to contain one complete telegram.
// If the string contains more than one telegram only the first is returned.
func Parse(telegram string) (t Telegram, err error) {
	var data string
	t.Prefix, t.Identification, data, t.CRC, err = parseTelegram(telegram)
	if err != nil {
		return
	}

	objects := ObjectRegexp.FindAllStringSubmatch(data, -1)
	t.Data = make(map[string][]string, len(objects))
	for _, object := range objects {
		t.Data[object[1]] = parseValues(object[2])
	}

	t.postProcess()
	return
}

// parseTelegram returns the parts of a telegram.
func parseTelegram(telegram string) (prefix, identification, data, crc string, err error) {
	parts := TelegramRegexp.FindStringSubmatch(telegram)
	if len(parts) != 5 {
		err = fmt.Errorf("invalid telegram")
		return
	}

	prefix = parts[1]
	identification = parts[2]
	data = parts[3]
	crc = parts[4]

	return
}

// parseValues splits the value component of an object.
func parseValues(values string) []string {
	valueMatches := valueRegexp.FindAllStringSubmatch(values, -1)
	valueStrings := make([]string, len(valueMatches))
	for i, v := range valueMatches {
		valueStrings[i] = v[1]
	}
	return valueStrings
}

// postProcess handles some meter quirks:
// - The measurement time is set if not present.
// - The gas meter values are set from a combined reading.
func (t *Telegram) postProcess() {
	// Fallback to current time in case it was not set
	if vs := t.Get(0, DateTimeStamp); vs == nil {
		t.Data[fmt.Sprintf(DateTimeStamp, 0)] = []string{time.Now().Format(DateFormat)}
	}

	// All gas meters
	for i := 1; i < 10; i++ {
		vs := t.Get(i, GasCombinedMeterValue)
		if len(vs) < 7 {
			break
		}
		t.Data[vs[4]] = []string{vs[0], vs[6] + "*" + vs[5]}
	}
}

// Get returns the values for a given data key.
func (t *Telegram) Get(dev int, key string) []string {
	v, ok := t.Data[fmt.Sprintf(key, dev)]
	if !ok || len(v) == 0 {
		return nil
	}
	return v
}

// GetString returns the first value for a given key as a string.
func (t *Telegram) GetString(dev int, key string) (string, error) {
	if v := t.Get(dev, key); len(v) >= 1 {
		return v[0], nil
	}

	return "", fmt.Errorf("unknown key %q", key)
}

// GetInt returns the first value for a given key as an integer.
func (t *Telegram) GetInt(dev int, key string) (value int, err error) {
	v, err := t.GetString(dev, key)
	if err != nil {
		return 0, err
	}

	return strconv.Atoi(v)
}

// GetFloat returns the first value for a given key as a float and (optional) unit.
func (t *Telegram) GetFloat(dev int, key string) (value float64, unit string, err error) {
	var v string
	v, err = t.GetString(dev, key)
	if err != nil {
		return
	}
	return parseMeasurement(v)
}

// GetTime returns the first value for a given key as an integer.
func (t *Telegram) GetTime(dev int, key string) (time.Time, error) {
	v, err := t.GetString(dev, key)
	if err != nil {
		return time.Time{}, err
	}
	return parseTime(v)
}

// GetMeterValue returns the OtherMeterValue for a given device.
func (t *Telegram) GetMeterValue(dev int) (ts time.Time, v float64, unit string, err error) {
	vs := t.Get(dev, OtherMeterValue)
	if len(vs) < 2 {
		err = fmt.Errorf("no valid value for device %v", dev)
		return
	}

	ts, err = parseTime(vs[0])
	if err != nil {
		return
	}

	v, unit, err = parseMeasurement(vs[1])
	return
}

// parseMeasurement parses a DMSR measurement with an attached unit.
func parseMeasurement(m string) (v float64, unit string, err error) {
	parts := strings.SplitN(m, "*", 2)
	if len(parts) >= 1 {
		v, err = strconv.ParseFloat(parts[0], 64)
	}
	if len(parts) >= 2 {
		unit = parts[1]
	}
	return
}

// parseTime parses a DSMR timestamp.
func parseTime(ts string) (time.Time, error) {
	if len(ts) < len(DateFormat) {
		return time.Time{}, fmt.Errorf("invalid timestamp: %q", ts)
	}
	return time.ParseInLocation(DateFormat, ts[:len(DateFormat)], time.Local)
}
