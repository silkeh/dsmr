package main

import (
	"bufio"
	"flag"
	"log"
	"os"
	"strconv"

	"github.com/influxdata/influxdb1-client/v2"
	"github.com/tarm/serial"
	"gitlab.com/silkeh/dsmr"
)

const (
	electricityMeasurement = "electricity_usage"
	gasMeasurement         = "gas_usage"
)

// newElectricityPoint creates an InfluxDB point for the electricity measurement from a DSMR telegram.
func newElectricityPoint(host string, meter int, telegram dsmr.Telegram) (*client.Point, error) {
	// Get time and active meter
	timestamp, _ := telegram.GetTime(0, dsmr.DateTimeStamp)
	activeMeter, _ := telegram.GetInt(0, dsmr.TariffIndicatorElectricity)

	// Get current usage if this meter is active
	var currentReceived, currentDelivered float64
	if activeMeter == meter {
		currentReceived, _, _ = telegram.GetFloat(0, dsmr.ActualElectricityPowerReceived)
		currentDelivered, _, _ = telegram.GetFloat(0, dsmr.ActualElectricityPowerDelivered)
	}

	// Get total usage
	var totalReceived, totalDelivered float64
	switch meter {
	case 1:
		totalReceived, _, _ = telegram.GetFloat(0, dsmr.MeterReadingElectricityDeliveredToClientTariff1)
		totalDelivered, _, _ = telegram.GetFloat(0, dsmr.MeterReadingElectricityDeliveredByClientTariff1)
	case 2:
		totalReceived, _, _ = telegram.GetFloat(0, dsmr.MeterReadingElectricityDeliveredToClientTariff2)
		totalDelivered, _, _ = telegram.GetFloat(0, dsmr.MeterReadingElectricityDeliveredByClientTariff2)
	}

	// return InfluxDB point
	return client.NewPoint(
		electricityMeasurement,
		map[string]string{
			"host":  host,
			"meter": strconv.Itoa(meter),
		},
		map[string]interface{}{
			"current_received":  currentReceived * 1000,  // convert from kW to W
			"current_delivered": currentDelivered * 1000, // convert from kW to W
			"total_received":    totalReceived,           // in kWh
			"total_delivered":   totalDelivered,          // in kwH
		},
		timestamp)
}

// newGasPoint creates an InfluxDB point for the gas measurement from a DSMR telegram.
func newGasPoint(host string, meter int, telegram dsmr.Telegram) (*client.Point, error) {
	// Get time and usage meter
	timestamp, usage, _, _ := telegram.GetMeterValue(meter)

	// return InfluxDB point
	return client.NewPoint(
		gasMeasurement,
		map[string]string{
			"host":  host,
			"meter": strconv.Itoa(meter),
		},
		map[string]interface{}{
			"total_received": usage, // in m^3
		},
		timestamp)
}

// getPoints returns all points for a DSMR telegram.
func getPoints(host, data string) ([]*client.Point, error) {
	telegram, err := dsmr.Parse(data)
	if err != nil {
		return nil, err
	}

	el1, err := newElectricityPoint(host, 1, telegram)
	if err != nil {
		return nil, err
	}

	el2, err := newElectricityPoint(host, 2, telegram)
	if err != nil {
		return nil, err
	}

	gas, err := newGasPoint(host, 1, telegram)
	if err != nil {
		return nil, err
	}

	return []*client.Point{el1, el2, gas}, nil
}

func main() {
	var influxHost, influxDatabase, influxUser, influxPass string
	var serialPort string
	host, _ := os.Hostname()

	flag.StringVar(&influxHost, "influx.host", "", "Hostname of the InfluxDB server")
	flag.StringVar(&influxDatabase, "influx.db", "", "Database on the InfluxDB server")
	flag.StringVar(&influxUser, "influx.username", "", "Username to authenticate to the InfluxDB server")
	flag.StringVar(&influxPass, "influx.password", "", "Password to authenticate to the InfluxDB server")
	flag.StringVar(&host, "host", host, "Hostname of this instance")
	flag.StringVar(&serialPort, "serial.port", "/dev/ttyS1", "Serial port to connect to")
	flag.Parse()

	// Open serial interface to read from
	ser, err := serial.OpenPort(&serial.Config{
		Name: serialPort, Baud: 9600, Size: 7, StopBits: 1, Parity: serial.ParityEven})
	if err != nil {
		log.Fatal("Unable to read serial: ", err)
	}

	// Create InfluxDB client
	influx, err := client.NewHTTPClient(client.HTTPConfig{
		Addr:     influxHost,
		Username: influxUser,
		Password: influxPass,
	})
	if err != nil {
		log.Fatal("Unable to connect to InfluxDB: ", err)
	}
	defer influx.Close()

	// Create interaction objects and start main loop
	bpConfig := client.BatchPointsConfig{Database: influxDatabase}
	reader := bufio.NewReader(ser)
	points := make([]*client.Point, 0)
	for {
		// Read until the end of the next telegram
		data, err := reader.ReadString('!')
		if err != nil {
			log.Fatal(err)
		}

		// Create points from received data
		newPoints, err := getPoints(host, data)
		if err != nil {
			log.Printf("Unable to parse data: %s", err)
			continue
		}
		points = append(points, newPoints...)

		// Create an InfluxDB batch
		bp, err := client.NewBatchPoints(bpConfig)
		if err != nil {
			log.Printf("Unable to create Influx point batch: %s", err)
			continue
		}
		bp.AddPoints(points)

		// Write to InfluxDB
		err = influx.Write(bp)
		if err != nil {
			log.Printf("Unable to write points to InfluxDB")
			continue
		}

		// Clear backed up points
		points = nil
	}
}
